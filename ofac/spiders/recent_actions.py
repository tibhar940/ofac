import re
import requests
import pandas as pd
from lxml import html
import scrapy
from ofac.items import PostItem


def parse_urls():
    """parse id's on the main page to crawl with default params (r1, r2)"""
    main_url = "https://home.treasury.gov/policy-issues/financial-sanctions/recent-actions"
    page = requests.get(main_url)
    tree = html.fromstring(page.content)
    urls = tree.xpath(
        "//div[starts-with(@class, $val1)]//div[starts-with(@class, $val2)]//h2/span/a/@href",
        val1="view view-ofac-recent-actions",
        val2="views-row"
    )
    urls = ["{0}/{1}".format(main_url, url.split("/")[-1]) for url in urls]
    return urls


class PostsSpider(scrapy.Spider):
    name = "recent_actions"

    def start_requests(self):
        # obtain urls
        urls = parse_urls()
        # print(urls)
        for url in urls:
            yield scrapy.Request(url, self.parse)

    def parse(self, response):

        item = PostItem()
        post = response.xpath("//div[@class=$val1]", val1="content")
        ts_str = post.xpath(
            "//div[starts-with(@class, $val1)]//div[@class=$val2]/text()",
            val1="field field--name-field-release-date",
            val2="field__item"
        ).get()
        item["ts"] = pd.to_datetime(ts_str)
        item["text"] = post.xpath(
            "//div[starts-with(@class, $val1)]//div[@class=$val2]",
            val1="field field--name-field-body",
            val2="field__item"
        ).get()
        yield item

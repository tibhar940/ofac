# ofac

Crawler for ofac sanctions lists based on Scrapy

## Install dependencies
```shell
pip install -r requirements/main.txt
pip install -r requirements/dev.txt
```

## How to
```shell
# get list of spiders
scrapy list

# run spider
scrapy crawl recent_actions -o data/recent_actions.jl
```
